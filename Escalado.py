#! /usr/bin/env python
#-*-coding: utf-8-*-

import cv2
import numpy as np

angle = 0
tx = 0
ty = 0
scale = 1.0



#############################################################


drawing = False #true if mouse is pressed
#mode = True # if True, draw rectangle. Press ’m’ to toggle to curve
ix,iy = -1,-1
fx,fy = -1,-1


def draw_circle ( event ,x , y, flags, param ) :
	global ix , iy, fx,fy , drawing
	if event == cv2.EVENT_LBUTTONDOWN:
		drawing = True
		ix , iy = x , y

	elif event == cv2.EVENT_LBUTTONUP:
		fx , fy = x , y
		drawing = False
		#if mode is True :
		cv2.rectangle(img, (ix,iy),(fx,fy),(100,100,100),1)
		for row in range(len(img)):
			for col in range(len(img[row])):
				if (ix < col < fx)and (iy < row < fy) :
					img_nueva[row][col] = img[row][col]
				else:
					img_nueva[row][col] = 0
		cv2.destroyAllWindows()
		cv2.imshow ('image',img_nueva)
		#cv2.waitKey(0)
		#else :
		#	cv2.circle(img,(x,y),5,(0,0,0),2)

	#elif event == cv2.EVENT_MOUSEMOVE:
	#	if drawing is True:
	#		cv2.rectangle(img, (ix ,iy),(fx ,fy),(0,0,0),2)


def translate(img_nueva, tx, ty):
	M = np.float32([[ 1, 0, tx],
			[0,1,ty]])


	shifted = cv2.warpAffine(img_nueva, M, (w,h))
	return shifted 

def rotate(dst, angle, center , scale):

	if center is None:
		center = (w/2 ,h/2)
	

	S = cv2.getRotationMatrix2D (center ,angle , scale)
	rotated = cv2.warpAffine(dst, S, (w, h))

	return rotated


#img = np.zeros((512, 512, 3),np.uint8)

img = cv2.imread('hoja.png')
img_nueva = cv2.imread('hoja.png')


cv2.namedWindow('image')
cv2.setMouseCallback('image',draw_circle)


cv2.imshow ('image',img)


while(1) :
	
	k = cv2.waitKey(1) & 0xFF
	if k == 113 : #Salir, letra q
		break

	if k == 103 : #Guardar recorte, letra g
		
		cv2.imwrite('recorte.png',img_nueva)

		
		#cv2.destroyWindow('image')

		
	if k == 101 : #Rotacion y traslacion, letra e
		angle = 26
		tx = 100
		ty = 100
		scale = 1

		(h,w) = (img_nueva.shape[0], img_nueva.shape[1])
		center = (w/2 , h/2)

		dst = translate(img_nueva, tx, ty)

		(h,w) = (dst.shape[0], dst.shape[1])
		center = (w/2 , h/2)

		Tras_Rot = rotate(dst, angle, center, scale) 

		cv2.imwrite('traslado_rotacion.png',Tras_Rot)
		
		cv2.destroyAllWindows()
		cv2.imshow ('image',Tras_Rot)
		#cv2.waitKey(0)

	if k == 115 : # Escalado, letra s
		angle = 0
		scale = 2

		(h,w) = (Tras_Rot.shape[0], Tras_Rot.shape[1])
		center = (w/2 , h/2)

		Escalado = rotate(Tras_Rot, angle, center , scale) 

		cv2.imwrite('escalado.png',Escalado)
		cv2.destroyAllWindows()
		cv2.imshow ('image',Escalado)
		

	if k == 114: # Recuperar foto inicial, letra r
		cv2.destroyAllWindows()
		img = cv2.imread('hoja.png')
		cv2.namedWindow('image')
		cv2.setMouseCallback('image',draw_circle)
		cv2.imshow ('image',img)
		
		
cv2.destroyAllWindows()


